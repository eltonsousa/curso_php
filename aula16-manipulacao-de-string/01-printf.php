<!DOCTYPE html>
<html>
    <head>
      <link rel="stylesheet" href="_css/estilo.css"/>
      <meta charset="UTF-8"/>
      <title>Curso de PHP - CursoemVideo.com</title>
    </head>
    <body>
        <div>
            <?php
                $produto = "Leite";
                $preco = 4.7;
                //echo "O $produto custa R$: ".number_format($preco,2);
                printf("O %s Custa R$: %.2f", $produto, $preco); // %s = string, %f = numero real, $.2 = duas casas decimais
            ?>
        </div>
    </body>
</html>
 