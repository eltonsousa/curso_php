<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="_css/estilo.css"/>
  <meta charset="UTF-8"/>
  <title>Curso de PHP - CursoemVideo.com</title>
</head>
<body>
<div>
    <?php
    $esc = isset($_GET["num"]) ? $_GET["num"] : 1;
    for ($contador = 1; $contador <= 10; $contador++) { // repetição "for"
        $res = $contador * $esc;
        echo "$esc X $contador = $res <br/>";
    }
    /*
    $i = 1;
    $c = $num;
    do {                                                // repetição "do while"
        $res = $i * $c;
        echo "$c X $i = $res <br/>";
        $i ++;
    } while ($i <= 10);
    */
    ?>
    <a href="aula03-calculadora01.php" class="botao">Voltar</a>
</div>
</body>
</html>
 