<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="_css/estilo.css"/>
  <meta charset="UTF-8"/>
  <title>Curso de PHP - CursoemVideo.com</title>
</head>
<body>
<div>
    <?php
        $v = isset($_GET["val"]) ? $_GET["val"] : 1;
        echo "<h1>Calculo Fatorial de <span class='foco'>$v</span></h1>";
        $c = $v;
        $fat = 1;
        do {
            $fat = $fat * $c;
            $c--;
        } while ($c >= 1);
        echo "<h2>$v! = <span class='foco'>$fat</span><h2/>";
    ?>
    <a href="aula02-fatorial.html" class="botao">Voltar</a>
</div>
</body>
</html>
 