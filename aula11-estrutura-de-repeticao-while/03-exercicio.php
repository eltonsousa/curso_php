<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="_css/estilo.css"/>
  <meta charset="UTF-8"/>
  <title>Curso de PHP - CursoemVideo.com</title>
</head>
<body>
<div>
    <?php
    $ini = $_GET["ini"];
    $fim = $_GET["fim"];
    $inc = $_GET["inc"];

    if ($ini <= $fim) {
        while ($ini <= $fim) {
            echo "$ini<br/>";
            $ini += $inc;
        }
    } else {
        while ($ini >= $fim) {
            echo "$ini<br/>";
            $ini -= $inc;
        }
    }
    ?>
    <a href="03-exercicio.html" class="botao">Voltar</a>
    <a href="http://localhost:8888/curso_PHP/" class="botao">Voltar para a raiz</a>
</div>
</body>
</html>
 