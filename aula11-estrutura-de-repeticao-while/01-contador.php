<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="_css/estilo.css"/>
  <meta charset="UTF-8"/>
  <title>Curso de PHP - CursoemVideo.com</title>
</head>
<body>
<div>
    <?php
        $contador = 1;
        while ($contador <= 10) {
                echo "$contador <br/>";
                //$contador += 1; // alterar o valor pra tirar ou acrescentar
                $contador ++; // mais prático ($var = $var + 1;)
                /*$contador = $contador + 1;*/ /* maneira simples */
    }
    ?>
</div>
</body>
</html>
 