<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="_css/estilo.css"/>
  <meta charset="UTF-8"/>
  <title>Curso de PHP - CursoemVideo.com</title>
</head>
<body>
<div>
    <?php
    $a = isset($_GET["ano"]) ? $_GET["ano"] : 1900;
    $i = date(Y) - $a;
    echo "Você nasceu em $a e tem $i anos.<br/>";
    if ($i < 16) {
        $tipovoto = "Não vota";
    }
    elseif (($i >= 16 && $i < 18) || ($i >= 65)) {
        $tipovoto = "Voto opcional";
    }
    else {
        $tipovoto = "Voto obrigatório";
    }
    echo "Para essa idade $tipovoto";
    ?>
    <a href="exercicio02.html">Voltar</a>
</div>
</body>
</html>
 