<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="_css/estilo.css"/>
    <meta charset="UTF-8"/>
    <title>Curso de PHP - CursoemVideo.com</title>
</head>
<body>
<div>
    <?php
    $num = isset($_GET["num"]) ? $_GET["num"] : 1;
    for ($c = 1; $c <= 10; $c++) {
        $r = $num * $c;
        echo "<span class='foco'>$num X $c = $r</span><br/>";
    }
    ?>
    <br/><a class="botao" href="javascript:history.go(-1)">Voltar</a>
    <a class="botao" href="http://localhost:8888/curso_PHP/">Sair para raiz</a>
</div>
</body>
</html>
