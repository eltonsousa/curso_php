<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="_css/estilo.css"/>
    <meta charset="UTF-8"/>
    <title>Curso de PHP - CursoemVideo.com</title>
</head>
<body>
<div>
    <form method="get" action="02-calculadora-for.php">
        <b>Calculadora em PHP com estutura de repetição 'for'</b>
        <span class="foco">Escolha o numero:</span> <select name="num">
            <?php
            for ($cont = 1; $cont <= 10; $cont++) {
                echo "<option> $cont</option>";
            }
            ?>
        </select>
        <input type="submit" value="Gerar" class="botao">
    </form>
</div>
</body>
</html>
