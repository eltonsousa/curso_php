<!DOCTYPE html>
<html lang="pt-br">
<head>
  <link rel="stylesheet" href="_css/estilo.css"/>
  <meta charset="UTF-8"/>
  <title>Curso de PHP - CursoemVideo.com</title>
</head>
<body>
<div>
    <?php
        # exemplo de porcentagem para aumento:
        $preco = $_GET["p"];
        echo "O preço do produto é : $preco";
        $preco = $preco +($preco*10/100); # maneira antiga de operadores de atribuição com variáveis iguais
        $preco += $preco*10/100; # maneira simplificada: variável recebe ela mesmo (Ex: $var = $var) passa a ser ( $var += )
        echo "<br/>o aumento de 10% será de R$: $preco";

        # exemplo para desconto:
    $preco = $_GET["p"];
    echo "<br/>O preço do produto é : $preco";
    $preco -= $preco*10/100;
    echo "<br/>o desconto de 10% será de R$: $preco";
    ?>
</div>
</body>
</html>
 