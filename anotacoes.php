<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8"/>
    <title>Anotações PHP</title>
</head>

<body>

<p>#==================================#</p><br/>
estrutra de repetição "while":
<?php
// anotações
// estrutra de repetição while
// exemplo contagem de um a 10
$cont = 1;
while ($cont <= 10) {
    echo "$cont ";
    $cont++; // maneira mais prática, o mesmo seria: "$cont = $cont + 1;" ou: "$cont += 1;"
}
?>

<p>#==================================#</p><br/>
contagem regressiva "while":
<?php
// exemplo contagem regressiva
$cont = 10; // contagem regressiva, inverte o valor do "$cont"
while ($cont >= 1) { // inverte o valor
    echo "$cont ";
    $cont--; // troca o sinal
}
?>

<p>#==================================#</p><br/>
contagem "do while":
<?php
$cont = 1;
do {
    echo "$cont ";
    $cont++;
} while ($cont <= 10);
?>

<p>#==================================#</p><br/>
contagem "for":
<?php
for ($i = 1; $i <= 10; $i++){
	echo "$i ";
}
?>
<p>#==================================#</p><br/>

</body>
</html>
