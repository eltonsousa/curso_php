<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="_css/estilo.css"/>
  <meta charset="UTF-8"/>
  <title>Curso de PHP - CursoemVideo.com</title>
</head>
<body>
<div>
    <?php
        /*Outras formas de inclusão, são 4:
        include --> inclui, caso falte o arquivo ele tenta continuar o script.
        require --> inclui, mas caso falte o arquivo ele para o script.
        include_once --> verifica se a função já foi incluida pra não repetir.
        require_once
        */
        include "funcoes.php";
        echo "<h1>Testando funçoes externas</h1>";
        ola();
        mostravalor(5);
        echo "<p>Fim</p>"
    ?>
</div>
</body>
</html>
 