<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="_css/estilo.css"/>
  <meta charset="UTF-8"/>
  <title>Curso de PHP - CursoemVideo.com</title>
</head>
<body>
<div>
    <?php
        function teste (&$x){ // passagem por parâmetro (p/ passagem por valor, retirar o '&')
            $x += 2;
            echo "<p>O valor de X é $x</p>";
        }
        $a = 3;
        teste($a);
        echo "<p>O valor de A é $a</p>"; // nesse caso $a vale 5 pois é passagem por parâmetro
    ?>
</div>
</body>
</html>
 