<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8"/>
  <link rel="stylesheet" href="_css/estilo.css"/>
  <title>Aula PHP | by Elton</title>
  <style>
      h1, h2 {
          text-shadow: 1px 1px 1px rgba(0,0,0,.5);
      }
      h2 {
          color: #7e050c;
      }
  </style>
</head>
<body>
<div>
    <?php
       $num1 = 3;
       $num2 = 2;
       $som = $num1 + $num2;
       echo "<h1>A soma de $num1 e $num2 é: $som</h1> <br/>";
       echo "<h2>By Elton Sousa<h2/>"
    ?>
</div>
</body>
</html>
 